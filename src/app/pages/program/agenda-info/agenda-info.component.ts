import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AgendaService } from '../../../services/api/agenda.service';
import { DateService } from '../../../services/api/date.service';
import { RoomService } from '../../../services/api/room.service';

import { forkJoin as observableForkJoin } from 'rxjs';
import { BannerService } from '../../../services/api/banner.service';
import { NgImageSliderComponent } from 'ng-image-slider';

@Component({
  selector: 'app-agenda-info',
  templateUrl: './agenda-info.component.html',
  styleUrls: ['./agenda-info.component.scss']
})
export class AgendaInfoComponent implements OnInit {
  @ViewChild('bannerSlider') bannerSlider: NgImageSliderComponent;
  public dates: any[] = []; // 날짜 목록
  public rooms: any[] = []; // 룸 목록
  public agenda: any; // 선택한 Agenda 정보
  public selected: any = {
    date: null,
    room: null
  };

  public banners: any[] = [];

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public agendaService: AgendaService,
    public dateService: DateService,
    public roomService: RoomService,
    private bannerService: BannerService
  ) {
    this.doInit();
  }

  ngOnInit(): void { }

  public doInit() {
    const observables = [this.getAgenda(), this.getDates(), this.getRooms()];
    observableForkJoin(observables).subscribe(res => {
      this.agenda = res[0];
      this.dates = res[1];
      this.rooms = res[2];
      this.selected.date = this.dates.find(date => {
        return date.id === this.route.snapshot.params['dateId']
      });
      this.selected.room = this.rooms.find(room => {
        return room.id === this.route.snapshot.params['roomId']
      });

      this.getBanners();
    });
  }

  /** 아젠다 상세 조회 */
  getAgenda() {
    return this.agendaService.findById(this.route.snapshot.params['agendaId']);
  }

  /** 날짜 목록 조회 */
  getDates() {
    return this.dateService.find();
  }

  /** 룸 목록 조회 */
  getRooms() {
    return this.roomService.find();
  }

  /**
   * 날짜 / 룸 선택
   * @param date 
   * @param room 
   */
  setAgendaList(date: any, room: any) {
    this.router.navigate([`/live`], { queryParams: { dateId: date.id, roomId: room.id } });
  }
  // ! -------------------  Banner Slider  -------------------
  // 슬라이드 배너 신경외과용이니 나중에 복사한다.
  /** 배너목록을 조회한다. */
  getBanners(): void {
    this.bannerService.find().subscribe(res => {
      if (res.live) {
        res.live.forEach(item => {
          const data = {
            link: item.link,
            thumbImage: item.photoUrl,
            alt: item.title,
          };
          this.banners.push(data);
        });
      }
      // const bannersLiveChat = [];
      // res.liveChat.forEach(item => {
      //   const data = {
      //     link: item.link,
      //     thumbImage: item.photoUrl,
      //     alt: item.title,
      //   };
      //   bannersLiveChat.push(data);
      // });

      // if (bannersLiveChat.length > 0) {
      //   this.bannersLiveChat = bannersLiveChat;
      // }
    });
  }

  // 광고 구좌 왼쪽 버튼 클릭
  slidePrev(target): void {
    this[target].prev();
  }
  // 광고 구좌 오른쪽 버튼 클릭
  slideNext(target): void {
    this[target].next();
  }
  // 광고 배너 클릭
  imageClick(index): void {
    if (this.banners[index] && this.banners[index].link) {
      window.open(this.banners[index].link);
    }
  }

}
