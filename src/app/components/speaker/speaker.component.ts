import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SpeakerService } from '../../../../src/app/services/api/speaker.service';

@Component({
  selector: 'app-speaker',
  templateUrl: './speaker.component.html',
  styleUrls: ['./speaker.component.scss']
})
export class SpeakerComponent implements OnInit {
  @Input('speaker') speaker: any;
  @Output('speakerDetailFn') detailFn = new EventEmitter(); // 자세히보기

  constructor(
    public speakerService: SpeakerService,
  ) { }

  ngOnInit(): void {
  }

}
